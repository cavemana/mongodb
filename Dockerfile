ARG IMAGE_VERSION=latest

FROM mongo:${IMAGE_VERSION} 

COPY ./init.js /docker-entrypoint-initdb.d/